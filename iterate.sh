#!/usr/bin/env bash

for dir in components/*; do

    if [ -d "$dir" ]; then
        gcloud builds submit --config=cloudbuild.yaml --substitutions=_PACKAGE_NAME=$dir .
		#echo $dir
    fi
done
